create table flowers
(
    id      serial primary key,
    flowers varchar(100),
    price   int not null

);
select*
from flowers;
insert into flowers
values (1, 'Розы', 100);
insert into flowers
values (2, 'Лилии', 50);
insert into flowers
values (3, 'Ромашки', 25);


create table clients
(
    id          serial primary key,
    name        varchar(100),
    phoneNumber Varchar(11)
);


select*
from clients;

insert into clients
values (1, 'Саша', 79119378912);
insert into clients
values (2, 'Сережа', 89239139112);
insert into clients
values (3, 'Вася', 89123991212);
insert into clients
values (4, 'Маша', 81291191281);
insert into clients
values (5, 'Яна', 8911223916);
insert into clients
values (6, 'Коля', 8912300121);



create table orders
(
    id             serial primary key,
    clients_id     integer REFERENCES clients,
    flowers_id     integer REFERENCES flowers,
    date_added     timestamp,
    flowers_amount int not null
        constraint che_flowers_amount_clients_orders
            check ( flowers_amount > 0 and flowers_amount < 1001)
);
select*
from orders;

insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (1, 1, 3, now() - interval '65d', 20);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (2, 2, 2, now() - interval '43d', 200);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (3, 3, 3, now() - interval '621d', 300);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (4, 4, 2, now() - interval '43d', 300);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (5, 4, 2, now(), 200);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (6, 1, 2, now() - interval '43d', 3);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (7, 6, 2, now() - interval '62d', 5);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (8, 3, 3, now() - interval '43d', 3);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (9, 1, 1, now(), 20);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (10, 2, 2, now() - interval '62d', 200);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (11, 3, 3, now() - interval '4d', 300);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (12, 3, 2, now() - interval '43d', 300);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (13, 4, 2, now() - interval '3d', 200);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (14, 1, 2, now() - interval '62d', 3);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (15, 6, 2, now() - interval '4d', 5);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (16, 3, 3, now() - interval '65d', 3);
insert into orders(id, clients_id, flowers_id, date_added, flowers_amount)
values (17, 1, 2, now(), 4);

--По идентификатору заказа получить данные заказа и данные клиента,
--создавшего этот заказ

select*
from orders o
         join clients c on c.id = o.clients_id
where o.id = 1;


--Получить данные всех заказов одного клиента по идентификатору
--клиента за последний месяц

select *
from orders o
where o.clients_id = 1
  and date_added >= now() - interval '1month';

--Найти заказ с максимальным количеством купленных цветов, вывести их
--название и количество

SELECT o.flowers_id, max(o.flowers_amount) as MAX_FLOWERS_AMOUNT
from orders o
         join flowers f on f.id = o.flowers_id
group by o.flowers_id
order by MAX_FLOWERS_AMOUNT desc
limit 1;