package Collection.pat3.task2;

import java.util.HashMap;
import java.util.Map;

public class task2 {
    public static void main(String[] args) {


        Map<String, Integer> map = new HashMap<>();
        map.put("key1", 123);
    }
}
        /*
        данный метод будет работать если значение есть в списке,если нет, то выведет null
        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> {
            System.out.println("inside computeFunction key = " + key + " oldValue " + oldValue);
            return oldValue+5;
        };

        Integer result = map.computeIfPresent("key1", computeFunction);
        System.out.println("result : " + result);
        System.out.println(map);


    }
}

         */
/* // выполняется когда нет ключа
        Function<String, Integer> computeFunction = (String key) -> {
            System.out.println("inside computeFunction key = " + key);
            return 754;
        };

        Integer result = map.computeIfAbsent("key5", computeFunction);
        System.out.println("result : " + result);
        System.out.println(map);

    }
}

 */
        /*
        BiFunction<String, Integer, Integer> computeFunction = (String key, Integer oldValue) -> {
            System.out.println("inside computeFunction key = " + key +  " oldValue = " + oldValue);
            return oldValue*2;
        };

        Integer result = map.compute("key1", computeFunction);
        System.out.println("result : " + result);
        System.out.println(map);
    }
}


         */


        /*
        Данная ситуация используется, когда происходить коллизия(столконовение одинаковых ключей)  по ключам
        map.put("key1", 123);

        BiFunction<Integer, Integer, Integer> mergeFunction = (Integer oldValue, Integer newValue) -> {
            System.out.println("inside mergeFunction: oldValue =" + oldValue + " newValue =" + newValue);
            return oldValue + newValue;
        };

        Integer result = map.merge("key1", 500, mergeFunction);
        System.out.println("result " + result);
        System.out.println("map " + map);

         */
