package Collection.pat3.task3sortedmap;

import java.util.SortedMap;
import java.util.TreeMap;

public class Task3 {
    public static void main(String[] args) {


        SortedMap<Integer, String> map = new TreeMap<>();

        for (int i = 1; i <= 5; i++) {
            map.put(i, "Value_" + i);
        }

        System.out.println("map " + map);
        Integer result = map.firstKey();
        System.out.println("firstKey " + result);
        System.out.println();
        SortedMap<Integer, String> result2 = map.headMap(4);
        System.out.println("headMap " + result2);
        System.out.println();
        SortedMap<Integer, String> result3 = map.subMap(2, 5);
        System.out.println("subMap " + result3);
        System.out.println();
        SortedMap<Integer, String> result4 = map.tailMap(4);
        System.out.println("tailMap " + result4);

    }
}
