package Collection.pat3.task1;

import java.util.*;

public class Aplication {
    public static void main(String[] args) {

        //добавление идет в том порядке, в котором написано в условии
        //Map<Integer, String> map = new LinkedHashMap<>();
        //  {-2=NEW Милая_Яночка777, -1=NEW Value_-1, 0=NEW Value_0, 1=NEW Value_1, 2=NEW Value_2, 4=NEW Value_4, 5=NEW Value_5}
        //произвольное добавление
        // Map<Integer, String> map = new HashMap<>();
        //map {-1=Value_-1, 0=Value_0, -2=Value_-2, 1=Value_1, 2=Value_2, 3=Value_3, 4=Value_4, 5=Value_5}
        //попорядку возрастания
        // Map<Integer, String> map = new TreeMap<>();

        Map<Integer, String> map = new TreeMap<>();
        for (int i = 5; i >= -2; --i) {
            map.put(i, "Value_" + i);
        }
        System.out.println("map " + map);
        System.out.println();
        boolean result = map.remove(4, "abc");
        System.out.println("result " + result);
        System.out.println();
        System.out.println("map " + map);
        System.out.println();
        System.out.println("метод containsKey возвращает true если ключ есть или false,если нет ");
        boolean result2 = map.containsKey(4);
        System.out.println(" result2 " + result2);
        System.out.println();
        System.out.println("метод containsValue возвращает true если значение есть или false,если нет ");
        boolean result3 = map.containsValue("Value_4");
        System.out.println(result3);
        System.out.println();

        boolean result4 = map.equals(map);
        System.out.println("result4 " + result4);
        System.out.println();

        String result5 = map.put(3, " Яночка Милая_!");
        System.out.println(result5);
        System.out.println("после метода put");
        System.out.println(map);
        System.out.println();
        String result6 = map.remove(3);
        System.out.println("после метода remove");
        System.out.println("result6 " + result6);
        System.out.println(map);
        System.out.println();
        boolean result7 = (map.replace(-2, "Value_-2", "Милая_Яночка777"));
        System.out.println("после метода replace");
        System.out.println("result7 " + result7);
        System.out.println(map);
        System.out.println(map.size());
        System.out.println();
        System.out.println("Возвращает ключи");


        System.out.println("Так работает метод Set<Integer> keys = map.keySet();");
        Set<Integer> keys = map.keySet();


        for (Integer key : keys) {
            System.out.println("key " + key + " Value_" + map.get(key));
        }


        System.out.println();
        System.out.println("Так работает метод Collection<String> values = map.values();");
        Collection<String> values = map.values();
        for (String value : values) {
            System.out.println("value " + value);
        }
        System.out.println();
        System.out.println("Так работает метод   Set<Map.Entry<Integer, String>> entries");
        Set<Map.Entry<Integer, String>> entries = map.entrySet();
        for (Map.Entry<Integer, String> entry : entries) {
            Integer key = entry.getKey();
            String value = entry.getValue();
            System.out.println("{" + key + ";" + value + "}");

            entry.setValue("NEW " + value);

        }
        System.out.println(map);

        System.out.println();

    }

}

