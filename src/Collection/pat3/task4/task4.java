package Collection.pat3.task4;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class task4 {
    public static void main(String[] args) {


        NavigableMap<Integer, String> map = new TreeMap<>();

        for (int i = 0; i <= 5; i++) {
            map.put(i, "Value_" + i);
        }
        System.out.println("map " + map);


        Integer lowerKey = map.lowerKey(3);
        Map.Entry<Integer, String> lowerEntry = map.lowerEntry(3);
        System.out.println("lowerKey " + lowerKey);
        System.out.println("lowerEntry " + lowerEntry);
        System.out.println();
//k>=
        Integer ceilingKey = map.ceilingKey(3);
        Map.Entry<Integer, String> ceilingEntry = map.ceilingEntry(3);
        System.out.println("ceilingKey " + ceilingKey);
        System.out.println("ceilingEntry " + ceilingEntry);
        System.out.println();

        Integer firstKey = map.firstKey();
        Map.Entry<Integer, String> firstEntry = map.firstEntry();
        System.out.println("firstKey " + firstKey);
        System.out.println("firstEntry " + firstEntry);
        System.out.println();

        //<=k максимальная пара
        Integer floorKey = map.floorKey(2);
        Map.Entry<Integer, String> floorEntry = map.floorEntry(2);
        System.out.println("floorKey " + floorKey);
        System.out.println("floorEntry " + floorEntry);
        System.out.println();
//k>
        Integer higherKey = map.higherKey(2);
        Map.Entry<Integer, String> higherEntry = map.higherEntry(2);
        System.out.println("higherKey " + higherKey);
        System.out.println("higherEntry " + higherEntry);
        System.out.println();


        Integer lastKey = map.lastKey();
        Map.Entry<Integer, String> lastEntry = map.lastEntry();
        System.out.println("lastKey " + lastKey);
        System.out.println("lastEntry " + lastEntry);
        System.out.println();

//между элементами включая ихи или нет
        NavigableMap<Integer, String> subMap = map.subMap(2, true, 4, false);
        System.out.println("subMap " + subMap);
        System.out.println();
        //до какого то элемента
        NavigableMap<Integer, String> headMap = map.headMap(4, false);
        System.out.println("headMap " + headMap);
        System.out.println();
        //c какого то числа
        NavigableMap<Integer, String> tailMap = map.tailMap(4, true);
        System.out.println("tailMap " + tailMap);
        System.out.println();
        //выводит в обратном порядке
        NavigableMap<Integer, String> descendingMap = map.descendingMap();
        System.out.println("descendingMap " + descendingMap);
        System.out.println();
    }
}
