package Collection.task5;

import java.util.*;

/*
        Реализовать метод, который принимает массив words и целое положительное число k.
        Необходимо вернуть k наиболее часто встречающихся слов..
        Результирующий массив должен быть отсортирован по убыванию частоты
        встречаемого слова. В случае одинакового количества частоты для слов, то
        отсортировать и выводить их по убыванию в лексикографическом порядке.


 */

public class ListWords {
    public static void main(String[] args) {

        ArrayList<String> words = new ArrayList<>();
        words.add("the");
        words.add("day");
        words.add("is");
        words.add("sunny");
        words.add("the");
        words.add("the");
        words.add("the");
        words.add("sunny");
        words.add("is");
        words.add("is");
        words.add("day");

        System.out.println(ListWords.getListWords(words));


    }

    private static Map<String, Integer> getListWords(List<String> words) {

        Map<String, Integer> word = new HashMap<>();
        for (String s : words) {
            int cnt = 1;
            if (word.containsKey(s)) {
                cnt = word.get(s);
                cnt++;
            }
            word.put(s, cnt);
        }

        return word;

    }
}
