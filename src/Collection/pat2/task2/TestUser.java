package Collection.pat2.task2;

import java.util.ArrayList;
import java.util.Comparator;

public class TestUser {

    public static void main(String[] args) {

        ArrayList<User> list = new ArrayList<>();
        list.add(new User("Sergey", 27, "Kaliningrad"));
        list.add(new User("Yana", 30, "Stavropol"));
        list.add(new User("Sasha", 27, "Sankt-Peterburg"));

        Comparator<User> comparator = new UserByAgeComporator();
        Comparator<User> comparatorByCity = new UserByAgeComporator();


        list.sort(comparatorByCity);

        System.out.println("comparator " + list);


    }

}
