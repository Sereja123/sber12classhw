package Collection.pat2.task2;

public class User {

    String name;
    int age;
    String city;

    public User(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    public int compareTo(User other) {

        if (this.age == other.age) {
            return 0;
        }
        if (this.age < other.age) {
            return -1;
        } else
            return 1;

    }


}
