package Collection.pat2.task2;

import java.util.Comparator;

public class UserByAgeComporator implements Comparator<User> {

    @Override
    public int compare(User lhs, User rhs) {
        if (lhs.getAge() == rhs.getAge()) {
            return 0;
        }
        if (lhs.getAge() < rhs.getAge()) {
            return -1;
        } else
            return 1;
    }
}
