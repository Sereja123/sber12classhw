package Collection.pat2.task2;

import java.util.Comparator;

public class UserByCityComporator implements Comparator<User> {


    @Override
    public int compare(User lhs, User rhs) {

        return lhs.getCity().compareToIgnoreCase(rhs.getCity());
    }
}
