package Collection.pat2.task1;

import java.util.ArrayList;

public class TestMan {
    public static void main(String[] args) {


        Man man = new Man("Sergey", "Shilo1233", "SanctPeterberg");
        Man man1 = new Man("Yana", "Yana1233", "SanctPeterberg");


        boolean equalsValue = man.equals(man1);
        System.out.println("equalsValue равно " + equalsValue);
        System.out.println("hashCode " + man.hashCode());
        System.out.println("hashCode " + man1.hashCode());


        ArrayList<Man> list = new ArrayList<>();
        list.add(man);
        list.add(man1);


        System.out.println(list.contains(new Man("Sergey", "Shilo1233", "SanctPeterberg")));


    }
}