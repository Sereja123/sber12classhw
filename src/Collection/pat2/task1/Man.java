package Collection.pat2.task1;

import java.util.Objects;

public class Man {

    private String name;
    private String nikename;
    private String city;


    public Man(String name, String nikename, String city) {
        this.name = name;
        this.nikename = nikename;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getNikename() {
        return nikename;
    }

    public String getCity() {
        return city;
    }


    @Override
    public String toString() {
        return "Man{" +
                "name='" + name + '\'' +
                ", nikename='" + nikename + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        //если ничего нет
        if (o == null) {
            return false;
        }
        //если кто то сравнивает объект сам с собой
        if (this == o) {
            return true;

        }
        //сравнине объекта одного и тогоже типа - сравнение одинаковых объектов // обджект уже не нал
        if (!this.getClass().equals(o.getClass())) {
            return true;
        }
        //мы можем объект приводим к данному типу, где мы сравниваем все поля на эквивалентность
        Man other = (Man) o;
        return name.equalsIgnoreCase(other.name) && nikename.equalsIgnoreCase(other.nikename) && city.equalsIgnoreCase(other.city);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name.compareToIgnoreCase(name), nikename, city.compareToIgnoreCase(city));


    }


}
