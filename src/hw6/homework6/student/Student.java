package hw6.homework6.student;

public class Student {
    private final static int GRADE_MIN = 0;
    private final static int GRADE_MAX = 5;
    private String name;
    private String surname;
    private int[] grades;


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return this.grades;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;

    }

    public void setNewGrade(int newGrade) {
        int countGrades = 0;
        if (newGrade > 0 && newGrade <= 5) {
            ++countGrades;
            grades[countGrades - 1] = newGrade;
        } else if (countGrades == GRADE_MAX && newGrade > 0 && newGrade <= 5) {
            for (int i = 1; i < grades.length; i++) {
                grades[i - 1] = grades[i];
            }
            grades[GRADE_MAX - 1] = newGrade;
        }
    }

    public int AverageGradeOfStudent() {
        int total = 0;
        int count = 0;
        int averange;
        for (int i = 0; i < grades.length; i++) {
            total += grades[i];
            count++;
        }
        averange = total / count;
        return averange;
    }


}



