package hw6.homework6.cat;

public class Cat {
    private String sleep;
    private String meow;
    private String eat;
    private String status;

    Cat(String sleep, String meow, String eat) {
        this.sleep = sleep;
        this.meow = meow;
        this.eat = eat;
    }

    String getSleep() {
        return sleep;
    }

    String getMeow() {
        return meow;
    }

    String getEat() {
        return eat;
    }

    public String getstatus() {
        String result;
        int v = (int) (1 + Math.random() * 4);
        switch (v) {
            case (1):
                result = getMeow();
                break;
            case (2):
                result = getEat();
                break;
            case (3):
                result = getSleep();
                break;
            default:
                result = "неопределенное состояние...";
                break;
        }
        return result;
    }
}

