package hw6.Theory.week7.task4;

public class Main {
    public static void main(String[] args) {
        System.out.println(FieldValidator.validateName("anna"));
        System.out.println(FieldValidator.validateName("Gleb"));
        System.out.println(FieldValidator.validateName("slkjdfl;"));
        System.out.println(FieldValidator.validateName("Anna"));
    }

}
