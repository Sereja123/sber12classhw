package hwException.task4;

public class MyArithmeticException extends Exception{

    MyArithmeticException(String message){
        super(message);
    }
}
