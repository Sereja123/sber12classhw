package hwException.task4;

class MyEvenNumber {
    private int n;

    MyEvenNumber(int value) throws MyArithmeticException {
        checkEvenNumber(value);
        this.n = value;

    }

    public int setN(int value) throws MyArithmeticException {
        checkEvenNumber(value);
        this.n = value;
        return value;
    }

    private int checkEvenNumber(int value) throws MyArithmeticException {
        if (value % 2 != 0) {
            throw new MyArithmeticException("Число должно быть четным");
        }
        return value;
    }


}

