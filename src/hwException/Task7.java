package hwException;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int list[] = new int[n];
        for (int i = 0; i < list.length; i++) {
            list[i] = scanner.nextInt();
        }
        selectionSort(list);
        System.out.println(list[0]+" "+ list[1]);
    }

    public static void selectionSort(int[] list) {
        for (int i = 0; i < list.length - 1; i++) {

            int currentMin = list[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < list.length; j++) {
                if (currentMin < list[j]) {
                    currentMin = list[j];
                    currentMinIndex = j;
                }
            }


            if (currentMinIndex != i) {
                list[currentMinIndex] = list[i];
                list[i] = currentMin;
            }
        }

    }
}