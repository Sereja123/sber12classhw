package hwException.task1;

public class NewJob {
    public boolean knowledge;
    public boolean skills;
    public boolean foreignLanguage;
    String namejobe;

    public NewJob(String namejobe) {
        this.namejobe = namejobe;
    }

    public void getKnowledge() {
        System.out.println("Знания получены");
        this.knowledge = true;
    }

    public void getSkills() {
        System.out.println("Уменя получены");
        this.skills = true;
    }

    public void getForeignLanguage() {
        System.out.println("иностранный язык выучен");
        this.foreignLanguage = true;
    }


    public void applay() throws MyCheckedException {
        System.out.println("Устриваемся на новую работу");
        if (knowledge && skills && foreignLanguage) {
            System.out.println("Можно писать резюме в " + namejobe);
        } else
            throw new MyCheckedException(namejobe + " не готовы пригласить на собеседование");
    }

    public static class MyCheckedException extends Exception {

        public MyCheckedException(String message) {
            super(message);
        }
    }

    public static class TestMyCheckedException extends NewJob {

        public TestMyCheckedException(String namejobe) {
            super(namejobe);
        }

        public static void main(String[] args) {

            NewJob jobe = new NewJob("Сбербанк");

            jobe.getKnowledge();
            jobe.getSkills();
            jobe.getForeignLanguage();
            try {
                jobe.applay();
            } catch (MyCheckedException ex) {
                System.out.println(ex.getMessage());
                System.out.println("Проверяем готовность к собеседованию:\n" + " иностранный язык: " + jobe.foreignLanguage +
                        "\n " + "умения: " + jobe.skills + "\n " + "знания:  " + jobe.knowledge);
            }
        }
    }
}

