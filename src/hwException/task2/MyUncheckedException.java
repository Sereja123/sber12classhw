package hwException.task2;

public class MyUncheckedException extends RuntimeException {

    MyUncheckedException(String message) {
        super(message);
    }

    public static class Array {
        public static void main(String[] args)throws MyUncheckedException {
            int number[] = {6, 4, 17, 15, 67,128};
            int denom[] = {2, 0, 4, 4, 0, 8};

            for (int i = 0; i < number.length; i++) {
                try {
                    System.out.println(number[i] + "/" + denom[i] + "равно " + number[i] / denom[i]);
                } catch (ArithmeticException exc) {
                    throw new MyUncheckedException("Попытка деления на ноль");

                }
            }
        }
    }
}
