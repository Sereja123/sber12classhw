package HW3;

import java.util.Scanner;

/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.

 */
public class task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int result = 0;
        int count = 1;
        for (int i = 0; i <count; i++) {
            System.out.println("введите отрицательное число");
            int x = scanner.nextInt();
            if (x>0){
                break;}
            result++;
            count++;
        }
        System.out.println(result);
    }
}
