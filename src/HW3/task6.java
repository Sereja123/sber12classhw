package HW3;

import java.util.Scanner;

public class task6 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            int m = 8;
            while (n > 0 || m > 0) {
                int p = n / m;
                if (m > 1) {
                    System.out.print(p + " ");
                } else {
                    System.out.println(p);
                }
                n = n - p * m;
                m = m / 2;
            }
        }
    }


