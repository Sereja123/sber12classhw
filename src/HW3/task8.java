package HW3;
/*\
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.
 */

import java.util.Scanner;

public class task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int p = scanner.nextInt();

        int result = 0;
        int count = 1;
        while (count <=n) {
            int a = scanner.nextInt();
            if (a > p)
                result += a;
            count++;
        }
        System.out.println(result);

    }
}
