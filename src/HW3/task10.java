package HW3;

import java.util.Scanner;

/*
Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
 */
public class task10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int x = scanner.nextInt();

        for (int i = 1; i <= x; i++) {
            for (int j = 1; j <= x - i; j++) {
                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * i - 1; j++) {
                System.out.print("*");
            }
            System.out.println();

            }
        for (int i = 1; i <= x; i++) {
            if (i != x) {
                System.out.print(" ");
            } else System.out.println("|");
        }
        }

    }




