package HW5.homeWork5;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        System.out.println(sum(n));
    }

    private static int sum(int n) {

        if (n < 10) {
            return n;
        }
        else {
            return n % 10 + sum(n / 10);
        }
    }
}
