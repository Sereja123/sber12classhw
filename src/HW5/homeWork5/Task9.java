package HW5.homeWork5;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        System.out.println(sum(n));
    }

    private static String sum(int n) {

        if (n < 10) {
            return Integer.toString(n);

        }
        else {
             return sum(n / 10) + " " + n % 10;
        }
    }
}
