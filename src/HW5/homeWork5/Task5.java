package HW5.homeWork5;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        boolean isSimmetric = true;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (a[i][j] != a[n - 1 - j][n - 1 - i]) {
                    isSimmetric = false;
                    break;
                }
            }
            if (!isSimmetric) break;
        }
        System.out.println(isSimmetric);
    }
}
