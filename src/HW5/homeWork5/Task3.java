package HW5.homeWork5;

import java.util.Scanner;


public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        char array[][] = new char[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                array[i][j] = '0';
            }
        }

        int Y= scanner.nextInt();//3
        int X = scanner.nextInt();//3
        array[X][Y] = 'K';

        if (X - 2 >= 0 && Y - 1 >= 0) {
            array[X - 2][Y - 1] = 'X';
        }
        if (X - 2 >= 0 && Y + 1 < N) {
            array[X - 2][Y + 1] = 'X';
        }
        if (X + 2 < N && Y - 1 >= 0) {
            array[X + 2][Y - 1] = 'X';
        }
        if (X + 2 < N && Y + 1 < N) {
            array[X + 2][Y + 1] = 'X';
        }
        if (X - 1 >= 0 && Y - 2 >= 0) {
            array[X - 1][Y - 2] = 'X';
        }
        if (X + 1 < N && Y - 2 >= 0) {
            array[X + 1][Y - 2] = 'X';
        }
        if (X - 1 > 0 && Y + 2 < N) {
            array[X - 1][Y + 2] = 'X';
        }
        if (X + 1 < N && Y + 2 < N) {
            array[X + 1][Y + 2] = 'X';
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (j != N - 1) {
                    System.out.print(array[i][j] + " ");
                } else
                    System.out.print(array[i][j]);
            }
            System.out.println();
        }
    }
}


