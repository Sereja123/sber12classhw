package HW1;
import java.util.Scanner;
public class Task4 {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60,
                MINUTES_PER_HOUR = 60, HOURS_PER_DAY = 24;

        long totalMinutes, currentMinute, totalHours, currentHour;
        Scanner scanner = new Scanner(System.in);
        long totalSeconds = scanner.nextInt();
        totalMinutes = totalSeconds / SECONDS_PER_MINUTE;
        currentMinute = totalMinutes % MINUTES_PER_HOUR;
        totalHours = totalMinutes / MINUTES_PER_HOUR;
        currentHour = totalHours % HOURS_PER_DAY;
        System.out.println("Текущее время равно " + currentHour + ":"
                + currentMinute + " GMT.");
    }
}
