package hw.stream.task3;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class task3 {
    public static void main(String[] args) {


        List<String> listWords = List.of("abc", "", "", "def", "qqq","abc", "", "", "def", "qqq","abc", "", "", "def", "qqq");
        Stream<String> stringStream = listWords.stream();

        int size = listWords.stream()

                .filter((p) -> !Objects.equals(p, "")).toList()
                .size();
        System.out.println(size);
    }

}


