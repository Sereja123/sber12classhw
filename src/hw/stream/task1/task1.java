package hw.stream.task1;

import java.util.stream.IntStream;

public class task1 {
    public static void main(String[] args) {
        int i;
        i = IntStream.rangeClosed(1, 100)
                .filter(x -> x % 2 == 0)
                .sum();
        System.out.println(i);
    }
}
