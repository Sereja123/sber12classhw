package hw.stream.task4;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//На вход подается список вещественных чисел. Необходимо отсортировать их по
//убыванию.
public class task4 {
    public static void main(String[] args) {

        List<Double> listNumbers = List.of(12.3, 1212.4, 12.4, 51.25, 15.313, 435.34);
        List<Double> integerStream = listNumbers.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(integerStream);


    }
}
