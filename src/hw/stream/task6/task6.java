package hw.stream.task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

//Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
public class task6 {
    public static void main(String[] args) {
        Set<Integer> inner1 = new HashSet<>(){{
            add(1);
            add(2);
            add(3);
        }};

        Set<Integer> inner2 = new HashSet<>(){{
            add(4);
            add(5);
            add(6);
        }};

        Set<Set<Integer>> set = new HashSet<>(){{
            add(inner1);
            add(inner2);
        }};

        System.out.println(flatMapExample(set)); // output [1, 2, 3, 4, 5, 6]

    }

    public static Set<Integer> flatMapExample(Set<Set<Integer>> set) {
        return set.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }
}

