package hw.stream.task2;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class task2 {
    public static void main(String[] args) {

        List<Integer> list = Collections.singletonList(Stream.of(1, 2, 3, 4, 5)
                .reduce((a, b) -> a * b).get());
        System.out.println(list);


    }


}

