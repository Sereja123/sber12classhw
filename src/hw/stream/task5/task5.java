package hw.stream.task5;

import java.util.List;
import java.util.stream.Collectors;

//На вход подается список непустых строк. Необходимо привести все символы строк к
//верхнему регистру и вывести их, разделяя запятой.
//Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ
public class task5 {
    public static void main(String[] args) {
        List<String> common = List.of("abc", "def", "qqq");
        System.out.println(formatAndPrint(common)); // output AAA,BBB,QQQ.
    }


    public static String formatAndPrint(List<String> list) {
        return list.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(",", "", "."));
    }


}
