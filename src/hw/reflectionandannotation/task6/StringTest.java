package hw.reflectionandannotation.task6;

import java.util.*;

/*
1. Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Строка является правильной скобочной последовательностью, если:
● Пустая строка является правильной скобочной последовательностью.
● Пусть S — правильная скобочная последовательность, тогда (S) есть
правильная скобочная последовательность.
● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
есть правильная скобочная последовательность.
 */
public class StringTest {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(isValid(s));
    }

    public static boolean isValid(String s) {

        Map<Character, Character> brackets = new HashMap<>();
        brackets.put(')', '(');
        brackets.put('}', '{');
        brackets.put(']', '[');
        char[] chars = s.toCharArray();
        Deque<Character> stack = new LinkedList<>();
        for (char elements : chars) {
            if (brackets.containsValue(elements)) {
                stack.push(elements);
            } else if (brackets.containsKey(elements)) {
                if (stack.isEmpty() || stack.pop() != brackets.get(elements)) {
                    return false;
                }
            }

        }
        return stack.isEmpty();
    }

}
