package hw.reflectionandannotation.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class ReflectionAndMethod {


    public static void main(String[] args) {

        Class<APrinter> ap = APrinter.class;

        //проверка на наличия исключения
        try {
            Method method = ap.getDeclaredMethod("print", int.class);
            System.out.println(method.getName());
            method.invoke(new APrinter(), 5);

        } catch (NoSuchMethodException e) {
            System.out.println("Thrown when a particular method cannot be found " + e);
        } catch (IllegalArgumentException e) {
            System.out.println("Thrown to indicate that a method has been passed" +
                    " an illegal or inappropriate argument." + e);
        } catch (InvocationTargetException e) {
            System.out.println("InvocationTargetException is a checked exception " +
                    "that wraps an exception thrown by an invoked method or constructor." + e);
        } catch (IllegalAccessException e) {
            System.out.println(e + "An IllegalAccessException is thrown when an application tries to reflectively create an instance (other than an array), set or get a field, or invoke a method, but the currently executing " +
                    "method does not have access to the definition of the specified class, field, method or constructor.");
        }
    }
}

class APrinter {
    int a;

    public void print(int a) {

        System.out.println(a);
    }

}