package hw.reflectionandannotation.task2;

import hw.reflectionandannotation.task1.isLike;

public class Like {
    private static void likes(Class<Test> testClass) {
        if (!testClass.isAnnotationPresent(isLike.class)) {
            return;
        }
        isLike isLike = testClass.getAnnotation(isLike.class);
        System.out.println(isLike.str());

    }

    public static void main(String[] args) {
        likes(Test.class);
    }
}

