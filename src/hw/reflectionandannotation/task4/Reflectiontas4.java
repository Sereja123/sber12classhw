package hw.reflectionandannotation.task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reflectiontas4 {
    interface A {

    }

    interface B {

    }

    interface J {

    }

    interface K {

    }

    class C implements A {

    }

    class D extends C implements B {

    }

    class F extends D implements J {

    }

    class T extends F implements K {

    }


    public static void main(String[] args) {

        System.out.println("Класс родитель класса T -> " + T.class.getSuperclass().getSimpleName());

        System.out.println("Список всех интерефейсов класса T ->");
        for (Class<?> itf : T.class.getInterfaces())
            System.out.println(itf.getSimpleName());

        List<Class<?>> interfaces = getAllInf(T.class);
        for (Class<?> anInterces : interfaces) {
            System.out.println(anInterces.getSimpleName());
        }

    }

    public static List<Class<?>> getAllInf(Class<?> inf) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (inf != Object.class) {
            interfaces.addAll(Arrays.asList(inf.getInterfaces()));
            inf = inf.getSuperclass();
        }
        return interfaces;
    }
}

