package hw.generics.task3;


import java.util.HashSet;
import java.util.Set;

/*
public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}

 */
public class PowerfulSet {

    PowerfulSet() {

    }

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        set1.retainAll(set2);
        return set1;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.addAll(set2);
        return set;

    }



    public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {

        Set<T> set = new HashSet<>(set1);
        set.removeAll(set2);
        return set;
    }
}




