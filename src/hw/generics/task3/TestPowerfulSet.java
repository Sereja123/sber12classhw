package hw.generics.task3;

import java.util.HashSet;
import java.util.Set;

public class TestPowerfulSet {
    public static void main(String[] args) {


        PowerfulSet set = new PowerfulSet();

        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        System.out.println(set.intersection(set1, set2));

        PowerfulSet anotherSet = new PowerfulSet();

        Set<Integer> set11 = new HashSet<>();
        set11.add(1);
        set11.add(2);
        set11.add(3);

        Set<Integer> set22 = new HashSet<>();
        set22.add(0);
        set22.add(1);
        set22.add(2);
        set22.add(4);

        System.out.println(anotherSet.union(set11, set22));
        PowerfulSet anotherSet1 = new PowerfulSet();

        Set<Integer> set111 = new HashSet<>();
        set111.add(1);
        set111.add(2);
        set111.add(3);



        Set<Integer> set222 = new HashSet<>();
        set222.add(0);
        set222.add(1);
        set222.add(2);
        set222.add(4);

        System.out.println(anotherSet.relativeComplement(set111, set222));

    }
}

