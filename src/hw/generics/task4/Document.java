package hw.generics.task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }




    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {

        Map<Integer, Document> documentMap = new HashMap<>();
        for (Document doc : documents) {
            documentMap.put(doc.getId(), doc);
        }
        return documentMap;
    }
}




