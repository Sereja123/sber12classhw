package hw.generics.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class T {
    public static void main(String[] args) {



        ArrayList<Integer> iter = new ArrayList<>();

        iter.add(1);
        iter.add(1);
        iter.add(2);
        iter.add(2);

        Set<Integer> anotherList = new HashSet<>(iter);
        System.out.println(anotherList);
    }
}
