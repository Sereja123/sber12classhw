package hw.exception;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int list[] = new int[n];
        for (int i = 0; i < list.length; i++) {
            list[i] = scanner.nextInt();
        }
        int key = scanner.nextInt();
        System.out.println(binarySearch(list, key));

    }
    public static int binarySearch(int[] list, int key) {
        int low = 0;
        int high = list.length - 1;

        while (high >= low) {
            int mid = (low + high) / 2;
            if (key < list[mid])
                high = mid - 1;
            else if (key == list[mid])
                return mid;
            else
                low = mid + 1;
        }

        return -1;
    }
}


