package hw.exception.task3;

import java.io.*;

public class FileReaderClose {
    private static final String PKG_DIRECTORY = "C:\\Users\\shili\\IdeaProjects\\sber12classhw\\src\\hw\\exception\\task3";
    private static final String OUTPUT_FILE_NAME = "output.txt";
    private static final String INPUT_FILE_NAME = "input.txt";


    public static void main(String[] args) throws FileNotFoundException {

        try {
            readAndWriteAndUpperCase();
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private static void readAndWriteAndUpperCase() throws IOException {
        File file = new File(PKG_DIRECTORY);
        try (FileReader Reader = new FileReader(new File(PKG_DIRECTORY + "/" + INPUT_FILE_NAME));
             Writer writer = new FileWriter(PKG_DIRECTORY + "/" + OUTPUT_FILE_NAME)) {

            int count;

            while ((count = Reader.read()) != -1) {
                char ch = (char) count;
                if (('a' <= ch) && (ch <= 'z')) {
                    ch -= 32;
                }
                writer.write(ch);

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}


