package HW4;

import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.

 */
public class task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] ai;
        ai = new double[n];
        for (int i = 0; i < ai.length; i++) {
            ai[i] = scanner.nextDouble();
        }
        double result = arithmeticMean(ai);
        System.out.println(result);
    }

    public static double arithmeticMean(double[] ai) {
        double result = 0;
        for (int i = 0; i < ai.length; i++) {
            result += ai[i];
        }
        return result / ai.length;
    }


}



