package HW4;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] aj = new int[m];
        for (int i = 0; i < m; i++) {
            aj[i] = scanner.nextInt();
        }
        boolean check = false;
        for (int i = 0; i < ai.length; i++) {
            if (ai[i] != aj[i]) {
                check = true;
                break;
            }
        }
        if (!check) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}
