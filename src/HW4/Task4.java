package HW4;
/*
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int result = 0;
        int etalon = ai[0];
        for (int i = 0; i < ai.length; i++) {
            if (etalon == ai[i]) {
                result++;

            } else {
                System.out.println(result + " " + ai[i - 1]);
                etalon = ai[i];
                result = 1;

            }


        }
        System.out.println(result + " " + etalon);

    }

}


