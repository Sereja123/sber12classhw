package HW4;

import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();


        int minModul = Math.abs(ai[0] - m);
        int max = ai[0];
        for (int i = 1; i < ai.length; i++) {
            int modul = Math.abs(m - ai[i]);
            if (minModul >= modul) {
                minModul = modul;
                if (max < ai[i]) {
                    max = ai[i];
                }

            }


        }
        System.out.println(max);
    }
}