package HW4;

import java.util.Scanner;

/*
1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();

        for (int i = 0; i < m; i++) {
            int temp = ai[ai.length-1];
            for (int j = ai.length - 1; j > 0; j--) {
                ai[j] = ai[j - 1];

            }
            ai[0] = temp;

        }
        for (int j : ai) {
            System.out.print(j + "");
        }
    }

}
