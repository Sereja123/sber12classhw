package HW4;

import java.util.Scanner;


public class Task6 {

    static final char[] russian = {'А', 'Б', 'В', 'Г', 'Д', 'E', 'Ж', 'З', 'И', 'Й', 'К', 'Л',
            'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч',
            'Ш', 'Щ', 'Ы','Ъ', 'Ь', 'Э', 'Ю', 'Я'};

    public static String[] morse ={".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.",
            "...", ". -", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();

        System.out.print(encodingToMorse(s));
    }

    private static String encodingToMorse(String s) {
        s = s.toUpperCase();
        String resultString = "";

        for (int i = 0; i < s.length(); i++) {
            char ai = s.charAt(i);
            for (int j = 0; j < russian.length-1; j++) {
                if (russian[j] == ai) {
                    if (russian[j] == russian.length - 1){
                        resultString = resultString + morse[j];
                }
                    resultString = resultString + morse[j]+" ";
                }
            }
        }
        return resultString;
    }
}