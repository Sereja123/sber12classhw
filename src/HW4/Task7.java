package HW4;

import java.util.Arrays;
import java.util.Scanner;

/*
(1 балл) На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        int n = scanner.nextInt();
        int[] ai = new int[n];
        for (int i = 0; i < n; i++) {
            ai[i] = scanner.nextInt();
        }

        int[] aj = new int[ai.length];
        for (int i = 0; i < ai.length; i++) {
            aj[i] = (int) Math.pow(ai[i], 2);

        }

        for (int i = 0; i < aj.length - 1; i++) {
            int currentMin = aj[i];
            int currentMinIndex = i;

            for (int j = i + 1; j < aj.length; j++) {
                if (currentMin > aj[j]) {
                    currentMin = aj[j];
                    currentMinIndex = j;
                }
            }
            if (currentMinIndex != i) {
                aj[currentMinIndex] = aj[i];
                aj[i] = currentMin;
            }
        }

        for (int i = 0; i < aj.length; i++) {
            if (aj[i] != aj.length) {
                System.out.print(aj[i] + " ");
            }
        }
    }

}

