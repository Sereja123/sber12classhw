package HW4;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Task10 {
    public static void main(String[] args) {
        int number = 0;
        do {
            System.out.println("Угадайте загаданное число ");
            System.out.println("Для окончания игры введите отрицательнео число");

            numberRandom(number);
        }while (number<0);
    }

    private static int numberRandom(int number) {
        Scanner scanner = new Scanner(System.in);
        int m;
        m = ThreadLocalRandom.current().nextInt(0, 1001);
        boolean isEqual =false;
        do {
            number = scanner.nextInt();
            if (number< 0) {
                return number;
            } else if (number < m)
                System.out.println(" число " + number + " меньше загаданного.");
            else if (number>m)
                System.out.println(" число " + number + " больше загаданного.");
            else
                System.out.println("Победа!");

        } while (!isEqual);

        return number;
    }


}




