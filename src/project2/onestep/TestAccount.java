package project2.onestep;

public class TestAccount {
    public static void main(String[] args) {
        Account account = new Account(1122, 300000);
        Account.setAnnualInterestRate(20);
        account.withdraw(50000);
        account.deposit(10000000000L);
        System.out.println("Баланс равен " + account.getBalance() + " руб.");
        System.out.println("Ежемесячный процент равен " + account.getMonthlyInterest() + " руб.");
        System.out.println("Этот счет был создан " + account.getDateCreated());

    }
}
