package project2.onestep;

import java.util.Date;

public class Account {
    private int id;
    private double balance;

    private static double annualInterestRate; // годовая процентная ставка

    private Date dateCreated; //дата созданяи счета

    public Account() {
        dateCreated = new Date();
    }
    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated = new Date();
    }
    /** Возвращает id */
    public int getId() {
        return id;
    }

    /** Возвращает баланс */
    public double getBalance() {
        return balance;
    }

    /** Возвращает годовую процентную ставку */
    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    /** Возвращает дату создания счета */
    public Date getDateCreated() {
        return dateCreated;
    }

    /** Присваивает новый id */
    public void setId(int id) {
        this.id = id;
    }

    /** Присваивает новый баланс */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /** Присваивает новую годовую процентную ставку */
    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }
    public double getMonthlyInterest() {
        return balance * (annualInterestRate / 1200);
    }
    public void withdraw(double amount) {  // метод снятия денег
        balance -= amount;
    }
    public void deposit(double amount) {   //метод пооплнение счета
        balance += amount;
    }



}
