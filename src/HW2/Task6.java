package HW2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();


        if (count<500)
            System.out.println("beginner");
        else if (500 <=count && count<1500)
            System.out.println("pre-intermediate");
        else if (1500<=count && count<2500)
            System.out.println("intermediate");
        else if (2500<=count && count<3500)
            System.out.println("upper-intermediate");
        else if (3500<=count)
            System.out.println("fluent");
    }
}
