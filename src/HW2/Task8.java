package HW2;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int k = s.lastIndexOf(' ');
        String firstpart = s.substring(0, k);
        String lastpart = s.substring(k + 1);
        System.out.println(firstpart + "\n" + lastpart);
    }

}
