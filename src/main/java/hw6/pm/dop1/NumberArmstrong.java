package hw6.pm.dop1;

public class NumberArmstrong {
    public static void main(String[] args) {

        int inputArmstrongNumber = 153;
        int tempNumber, digit, digitCubeSum = 0;

        tempNumber = inputArmstrongNumber;
        while (tempNumber != 0) {


            System.out.println("Current Number is " + tempNumber);
            digit = tempNumber % 10;
            System.out.println("Current Digit is " + digit);

            digitCubeSum = digitCubeSum + digit * digit * digit;
            System.out.println("Current digitCubeSum is " + digitCubeSum);
            tempNumber /= 10;

        }


        if (digitCubeSum == inputArmstrongNumber)
            System.out.println(inputArmstrongNumber + " is an Armstrong Number");
        else
            System.out.println(inputArmstrongNumber + " is not an Armstrong Number");

    }
}
