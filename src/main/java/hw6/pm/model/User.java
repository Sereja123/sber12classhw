package hw6.pm.model;

import lombok.Data;

@Data
public class User {
    private int id;
    private String surname;
    private String name;
    private String date;
    private int number;
    private String mail;
    private String books_title;

}
