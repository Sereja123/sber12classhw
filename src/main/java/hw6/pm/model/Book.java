package hw6.pm.model;

import lombok.Data;

import java.util.Date;
//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
//@ToString


@Data
public class Book {
    private Long id;
    private String title;
    private String author;
    private Date addedDate;
}
