package hw6.pm.mapper;

import hw6.pm.model.Book;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BookMapper {

    public Book toBook(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        System.out.println(resultSet.getInt("id"));
        System.out.println(resultSet.getString("title"));
        System.out.println(resultSet.getString("author"));
        System.out.println(resultSet.getDate("date_added"));
        return book;
    }

    public List<Book> toBookList(ResultSet resultSet) throws SQLException {
        List<Book> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(toBook(resultSet));
        }
        return list;

    }
}
