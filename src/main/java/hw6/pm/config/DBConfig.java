package hw6.pm.config;

import hw6.pm.dao.Book12DAO;
import hw6.pm.dao.User12DAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static hw6.pm.DBConstants.*;

@Configuration
public class DBConfig {
    @Bean
    @Scope("singleton")
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB_NAME,
                USER,
                PASSWORD);
    }
    @Bean
    @Scope("prototype")
    public Book12DAO bookDAO() throws SQLException {
        return new Book12DAO(connection());
    }

    @Bean
    @Scope("prototype")
    public User12DAO userDAO() throws SQLException {
        return new User12DAO(connection());
    }


}


