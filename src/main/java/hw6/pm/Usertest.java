package hw6.pm;

import hw6.pm.dao.Book1DAO;
import hw6.pm.dao.User1DAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Component
public class Usertest implements CommandLineRunner {

    @Qualifier("user12DAO")
    @Autowired
    private final User1DAO user1DAO;
    @Qualifier("book12DAO")
    @Autowired
    private final Book1DAO book1DAO;

    public Usertest(@Qualifier("user12DAO") User1DAO user1DAO, @Qualifier("book12DAO") Book1DAO book1DAO) {
        this.user1DAO = user1DAO;
        this.book1DAO = book1DAO;
    }


    public static void main(String[] args) {
        SpringApplication.run(Usertest.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        user1DAO.createTableUsers();
        // book1DAO.createTableBooks();
        //   book1DAO.addBooks("Над пропастью во ржи");
//        book1DAO.addBooks("Горе от ума");
//        book1DAO.addBooks("собрание сочинений");
        //user1DAO.addUser("Shilin","Sergey","23.12.1994",39210,"skulsad@sadad.com","Над пропастью во ржи");

      //  user1DAO.getByNumber(39210);

        user1DAO.getBooksByTelephoneNumber(39210);
    }
}

