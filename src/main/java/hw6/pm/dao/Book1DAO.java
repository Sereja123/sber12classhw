package hw6.pm.dao;

import hw6.pm.model.Book;

import java.sql.SQLException;
import java.util.List;

public interface Book1DAO {
    public void createTableBooks() throws SQLException;
    public void addBooks( String title);

    public List<Book> getByTitle(List<String>  booksTitles) throws SQLException;

}
