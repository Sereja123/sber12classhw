package hw6.pm.dao;


import hw6.pm.mapper.BookMapper;
import hw6.pm.model.Book;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class Book12DAO implements Book1DAO {
    @Autowired

    private Connection connection;
    @Autowired
    private BookMapper bookMapper =new BookMapper();

    public Book12DAO(Connection connection) {
        this.connection = connection;
    }

    public void createTableBooks() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "create table if not exists books(" +
                        "id serial primary key," +
                        "title varchar(100)," +
                        "author varchar(100)," +
                        "date_added timestamp);");

        preparedStatement.execute();
    }

    @Override
    public void addBooks(String title) {
        Book book = new Book();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO books(title )VALUES(?)");
            preparedStatement.setString(1, title);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> getByTitle(List<String> booksTitles) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from books where title = ?");
        List<Book> bookList = new ArrayList<>();
        for (String bookTitle : booksTitles) {
            preparedStatement.setString(1, bookTitle);
            bookList.addAll(bookMapper.toBookList(preparedStatement.executeQuery()));
        }
        return bookList;
    }
}

