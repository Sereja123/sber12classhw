package hw6.pm.dao;

import hw6.pm.model.Book;
import hw6.pm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface User1DAO {
    public void createTableUsers() throws SQLException;
    public void addUser(String surname, String name, String date, int number, String mail, String books_title) throws SQLException;

    public User getByNumber(int number) throws SQLException;
    public List<Book> getBooksByTelephoneNumber(int number) throws SQLException;

}
