package hw6.pm.dao;

import hw6.pm.model.Book;
import hw6.pm.model.User;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class User12DAO implements User1DAO {
    @Autowired
    private Connection connection;


    public User12DAO(Connection connection) {
        this.connection = connection;
    }


    public void createTableUsers() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "create table if not exists users(" +
                        "name varchar(100)," +
                        "surname varchar(100)," +
                        "date varchar(255)," +
                        "number int primary key," +
                        "mail varchar(100)," +
                        "books_title varchar(255));");

        preparedStatement.execute();
    }
    @Override
    public void addUser(String name,
                        String surname,
                        String date,
                        int number,
                        String mail,
                        String books_title) throws SQLException {
        User user = new User();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO users(surname," +
                            " name," +
                            " date," +
                            " number," +
                            " mail," +
                            " books_title)" +
                            " VALUES(?,?,?,?,?,?)");
            preparedStatement.setString(1, surname);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, date);
            preparedStatement.setInt(4, number);
            preparedStatement.setString(5, mail);
            preparedStatement.setString(6, books_title);
             preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public User getByNumber(int number) throws SQLException {
        User user = new User();
        PreparedStatement preparedStatement = connection.prepareStatement
                ("SELECT*from users where number =?");
        preparedStatement.setInt(1, number);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {

            user.setSurname(resultSet.getString("surname"));
            user.setName(resultSet.getString("name"));
            user.setDate(resultSet.getString("date"));
            user.setNumber(resultSet.getInt("number"));
            user.setMail(resultSet.getString("mail"));
            user.setBooks_title(resultSet.getString("books_title"));
        }
        return user;
    }
    @Override
    public List<Book> getBooksByTelephoneNumber(int number) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select books_title from users where number = ?;");

        preparedStatement.setInt(1,number);
        ResultSet resultSet = preparedStatement.executeQuery();

        List<String> booksTitles = new ArrayList<>();

        while (resultSet.next()) {
            String stringOfBookTitles = resultSet.getString("books_title");
            booksTitles = Arrays.asList(stringOfBookTitles.split(","));
            booksTitles.replaceAll(String::trim);
        }

        Book12DAO book12DAO = new Book12DAO(connection);

        return book12DAO.getByTitle(booksTitles);
    }




}
