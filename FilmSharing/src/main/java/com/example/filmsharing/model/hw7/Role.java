package com.example.filmsharing.model.hw7;

import lombok.*;

import javax.persistence.*;

@Entity
@Table (name = "roles")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;

}
