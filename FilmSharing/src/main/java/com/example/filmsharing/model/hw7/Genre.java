package com.example.filmsharing.model.hw7;

public enum Genre {
    FANTASY("Фантастика "),
    SCIENCE_FICTION("Научиная фантастика"),
    DRAMA("Драмма"),
    NOVEL("Роман");

    private final String genreText;


    Genre(String genreText) {
        this.genreText = genreText;
    }

    public String getGenreText() {
        return this.genreText;
    }
}
