package com.example.filmsharing.model.hw7;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Director")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Directors {
    @Setter(AccessLevel.NONE)
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "directorsfio")
    private String directors_fio;
    @Column(name = "position")
    private String position;

    @ManyToMany(
            mappedBy = "films",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Film> films = new HashSet<>();
}




