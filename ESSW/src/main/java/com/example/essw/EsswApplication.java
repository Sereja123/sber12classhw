package com.example.essw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsswApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsswApplication.class, args);
    }

}
